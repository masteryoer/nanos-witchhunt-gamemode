local gameTimer = nil
local Config = {}
Config.StartCountdown = 15

HUD = nil

-- How much stamina does the player start with
local stamina = 100;

-- Pre-start timer
Events.Subscribe("preStartGame", function (duration)
  preTimer = duration * 60
end)

-- Actually start the game
Events.Subscribe("startGame", function(duration)
    gameTimer = duration * 60
    startGame()
end)

Package.Subscribe("Load", function()
    HUD = WebUI("HUD","file:///UI/index.html")
end)

-- Start the game, do the countdown timer (move to server side once we are done)
function startGame()    
    HUD:CallEvent("ShowGameTimer")
    timer = Timer.SetInterval(function()
        if gameTimer > 0 then
            gameTimer = gameTimer - 1
            Package.Log("Time left ".. displayTime(gameTimer))
	        HUD:CallEvent("UpdateGameTime",displayTime(gameTimer))
        else
            endGame()
        end
    end,1000)
end

-- Function to end the game
function endGame()
    HUD:CallEvent("HideGameTimer")
    Timer.ClearInterval(timer)
    Package.Log("Game over!")
    -- Clear up the sprint interval
    sprintTimer.clearInterval()
end

Canvas.Subscribe("Update", function(self, width, height)
    self.DrawText(gameTimer, Vector2D(width / 2, height / 2))
    Canvas.Repaint()
end)

function displayTime(time)
    local minutes = math.floor(time/60)
    local seconds = time % 60
    return string.format("%02d:%02d", minutes, seconds)
end

-- Do the HUD and client side stuff for when a player dies
Events.Subscribe("CharacterDeath", function(character, role)
    if role == Config.roles[role] then
        -- We were a survivor
        EndGame()
    else
        -- We were the witch, let's immediately end the game.
        EndGame()
    end
end)

-- Debug this
Player.Subscribe("VOIP", function(self, is_talking) 
    HUD:CallEvent("StartVOIP",is_talking)
end)

-- Temporary functionality to enable the HUD
Events.Subscribe("ShowHud", function() 
    HUD:CallEvent("showTownsfolkHUD")
end)

-- Timer to make sure the player can only sprint when their stamina is up (NEEDS WORK)
local sprintTimer = Timer.SetInterval(function() 
    if (gameStarted) then
        local player = Client.GetLocalPlayer()
        local character = player:GetControlledCharacter()

        if character:GetGaitMode() == 2 then
        stamina = math.floor(stamina - 5)
        else
        stamina = math.floor(stamina + 1)
        end

        if stamina < 0 then
        stamina = 0
        elseif stamina > 100 then
        stamina = 100
        end

        if stamina == 0 then
            Events.CallRemote("playerCanSprint", false)
        else
            Events.CallRemote("playerCanSprint", true)
        end
    end

    --HUD:CallEvent("UpdateStamina",stamina)
end, 1)

-- Handle the display of notifications
Events.Subscribe("showNotification", function(message,title,type)
    HUD:CallEvent("AddNotification",message,title,type)
end)