Events.Subscribe("showWitchHUD", function() {
  console.log("Got it from here");
  $('#witch-hud').show();
  $('#townsfolk-hud').hide();
  $('#spectator-hud').hide();
});

Events.Subscribe("showTownsfolkHUD", function() {
  $('#witch-hud').hide();
  $('#townsfolk-hud').show();
  $('#spectator-hud').hide();
});

Events.Subscribe("showSpectatorHUD", function() {
  $('#witch-hud').hide();
  $('#townsfolk-hud').hide();
  $('#spectator-hud').show();
});

Events.Subscribe("UpdateGameTime",function(time) {
    $('#timer').html(time);
});

Events.Subscribe("UpdateTownsfolkCount",function(count) {
    $('#counter .count').html(count);
});

Events.Subscribe("UpdateRitualProgress", function(progress){
    $('#ritual-progress').css('style: '+ progress +'%;');
});

Events.Subscribe("UpdateMapName", function(mapName){
    $('#map-name').html(mapName);
});

Events.Subscribe("UpdateWhoSpectating",function(playerName, role) {
  $('#spectating-name .player-name').html(playerName);
  $('#spectating-name').removeAttr('class');
  $('#spectating-name').attr('class','playername '+ role);
});

Events.Subscribe("AddNotification", function(message, title, type) {
  console.log("Got notification call");
  var audio = new Audio('notification.wav');
  var content = $('<div class="notification '+ type +'"><span class="title">'+ title +'</span><span class="message">'+ message +'</span></div>');
  $('#notifications').append(content);
  audio.play();

  setTimeout(function(){
    $(content).remove();
  }, 10000)
});

Events.Subscribe("StartVOIP",function(isTalking) {
  if (isTalking) {
    $('#talking').addClass('istalking');
  }
  else {
    $('#talking').removeClass('istalking');
  }
});


Events.Subscribe("UpdateStamina",function(stamina) {
  console.log("Got update stamina event");
  $('#stamina').attr('style','width: '+ stamina +'%;');
});