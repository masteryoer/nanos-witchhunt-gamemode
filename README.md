# Nanos WitchHunt Game Mode

WitchHunt is a new take on the classic versus game mode. Play as a witch and lay traps and hunt down townsfolk and kill them, or play as a townsfolk and gather the required materials to create the banishing ritual and banish the witch.

Hunt or be hunted!

## Make sure to get the map pack!

https://gitlab.com/masteryoer/witchhunt-maps
