local gameStarted = false

local Config = {}

Config.GameLength = 2 -- How many minutes should the game take?

-- Make sure that when /startgame is ran, we can start the game
Server.Subscribe("Chat", function(text, player)
  if (text == '/startgame') then
    if gameStarted then
      Events.CallRemote("showNotification", player, 'A game is already in progress!','Error','error')
    elseif Player.GetCount() < 1 then
      Events.CallRemote("showNotification", player, 'Not enough players to start!','Error','error')
    else
      StartGame()
    end
  end

  -- Only for debugging. Remove this all later.
  if (text == '/showhud') then
    ShowHud()
  end

  if (text == '/addnotification') then
    Events.BroadcastRemote("AddNotification")
  end
end)

-- Do the actual start game stuff here
function StartGame()
  Events.BroadcastRemote("showNotification", 'A game was started! Prepare to begin.','Game Started!','success')
end


-- When a character dies
Character.Subscribe("Death", function(character)
  local player = player:GetValue("Player")
  local character = player:GetControlledCharacter()
  local team = player:GetTeam()

  if (team == 1) then
    EndGame()
  else
    if (Server:GetValue("playersLeft") -1) == 0 then
      EndGame()
    else
      -- Player died, add them to the spectator team
      character:SetTeam(3)
      player:SetVOIPChannel(3)
      
      player:SetValue("isDead",true)

      -- Unposses the player after a few seconds
      Time.Bind(
        Timer.SetTimeout(function(p)
          p:UnPosses()
          end, 2000, player), 
        player)

        --Events.BroadcastRemote("CharacterDeath", character, player:getValue("Role"))
    end
  end
end)

-- Function that checks win condition, then sends the game
function EndGame()
  Events.BroadcastRemote("showNotification", 'The game has finished!','Game Over!','error')
end

-- Function to spawn a Character to a player
function SpawnCharacter(player)
    -- Spawns a Character at position 0, 0, 0 with default's constructor parameters
    -- local new_character = Character(Vector(0, 0, 0), Rotator(0, 0, 0), "nanos-world::SK_Male")
    local new_character = Character(Vector(0, 0, 0), Rotator(0, 0, 0), "nanos-witchhunt-assets::Farmer")

    -- Possess the new Character
    player:Possess(new_character)

    -- Disable the ability of this player to punch
    new_character:SetCanPunch(false)
    
    -- If a game is in progress, the player joins as a spectator and is added to the spectator voice channel
    -- Otherwise, set them to the global team channel and VOIP channel (4)
    if (gameStarted) then
      new_character:SetTeam(3)
      player:SetVOIPChannel(3)
    else
      new_character:SetTeam(4)
      player:SetVOIPChannel(4)
    end
end

-- Witches can't die by normal means, so they should never be dead
function checkWinCondition()
  local playersDead = 0

  for k, player in pairs(Player.GetAll()) do
    if Player:GetValue("isDead") == true then
      playersDead = playersDead + 1
    end
  end

  -- Witch won!
  if playersDead == totalPlayers then
    EndGame()
  end
end

-- Subscribes to an Event which is triggered when Players join the server (i.e. Spawn)
Player.Subscribe("Spawn", SpawnCharacter)
-- Iterates for all already connected players and give them a Character as well
-- This will make sure you also get a Character when you reload the package
for k, player in pairs(Player.GetAll()) do
    SpawnCharacter(player)
end

Events.Subscribe("playerCanSprint", function(player, canSprint)
  character = player:GetControlledCharacter()
  character:SetCanSprint(canSprint)
end)
--[[ DEBUG STUFF ]]--

function ShowHud()
  Events.BroadcastRemote("ShowHud")
end
