Config = {}

Config.GameLength = 20 -- How many minutes is the game length?
Config.PreGameLength = 60 -- How many seconds is the pre-game length?
Config.PostGameLength = 60 -- How many seconds is the post-game length?
Config.MinPlayers = 2 -- What is the minimum number of players needed to start a game?

Config.Roles = {
    WITCH = 1,
    TOWNSFOLK = 2
}